import {LoginForm, RegistrationForm} from './components';
import auth from './reducer';

export {LoginForm, RegistrationForm, auth};