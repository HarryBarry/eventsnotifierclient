import axios from "axios";
import cookies from 'js-cookie';
import * as types from "../constant";

import {
  API_URL
} from '../../../config';

export function checkEmailLogin(email) {
  return dispatch => {
    const request = axios.post(`${API_URL}/checkEmailLogin`, { email: email });
    request
      .then(({data}) => {
        dispatch({
          type: types.USER_EXISTS,
          payload: data.response.user
        });
      })
      .catch(({error}) => {
        console.log(error);
      })
  }
}

export function checkEmailRegistration(email) {
  return dispatch => {
    const request = axios.post(`${API_URL}/checkEmailRegistration`, { email: email });
    request
      .then(({data}) => {
        dispatch({
          type: types.USER_EXISTS,
          payload: data.response.user
        });
      })
      .catch(({error}) => {
        console.log(error);
      })
  }
}

export function signIn(email, password) {
  return dispatch => {
    const request = axios.post(`${API_URL}/signin`, { email: email, password: password});
    request
      .then(({data}) => {
        if (data.response.user) {
          cookies.set('access_token', data.response.user.token);
          dispatch({
            type: types.AUTH,
            payload: data.response
          });
        } else {
          cookies.set('access_token', '');
          dispatch({
            type: types.AUTH,
            payload: data.response
          });
        }
      })
      .catch(({error}) => {
        console.log(error);
      })
  }
}

export function signUp(email, password) {
  return dispatch => {
    const request = axios.post(`${API_URL}/signup`, { email: email, password: password, name: name });
    request
      .then(({data}) => {
        if (data.response.user) {
          cookies.set('access_token', data.response.user.token);
          dispatch({
            type: types.AUTH,
            payload: data.response
          });
        } else {
          cookies.set('access_token', '');
          dispatch({
            type: types.AUTH,
            payload: data.response
          });
        }
      })
      .catch(({error}) => {
        console.log(error);
      })
  }
}

export function clearCredentials() {
  return dispatch => {
    dispatch({
      type: types.USER_EXISTS,
      payload: { user: null, user_exists: null, clientError: null }
    });
  }
}