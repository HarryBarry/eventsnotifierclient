import {combineReducers} from "redux";
import {auth} from '../widgets'

const rootReducer = combineReducers({
  auth
});

export default rootReducer;
