import React, {Component} from "react";
import {Link} from 'react-router';
import {connect} from "react-redux";

// Actions
import {signUp, checkEmailRegistration, clearCredentials} from '../../action';

// Utils
import validateEmail from '../../util';

import {
  SCSS_ROOT,
  SCSS_LOGIN_FORM,
  SCSS_HEADER,
  SCSS_BODY,
  SCSS_USER_LOGO,
  SCSS_INPUT,
  SCSS_ICON_WRAP,
  SCSS_ICON_PASSWORD,
  SCSS_CLIENT_ERROR,
  SCSS_MESSAGE,
  SCSS_BTN,
  SCSS_SIGNUP
} from './scss/index.scss';


@connect(({auth}) => ({...auth}), {signUp, checkEmailRegistration, clearCredentials})

class RegistrationForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nameVerified: null,
      password: null,
      passwordVerified: null,
      repeatPasswordVerified: null,
      isEnterPassword: false
    }
  }

  signUp(e) {
    const {signUp} = this.props;
    e.preventDefault(e);
    let email = e.target.email.value;
    let password = e.target.password.value;
    let name = e.target.name.value;
    signUp(email, password, name);
  }

  isExists(propIsExists) {
    if (propIsExists === true) {
      return ({'backgroundColor': 'red', 'color': '#fff'});
    } else if (propIsExists === false) {
      return ({'backgroundColor': '#00B200', 'color': '#fff'});
    } else {
      return ({'backgroundColor': '#E3E8EE', 'color': '#5a5a5a'});
    }
  }

  isNotExists(propIsExists) {
    if (propIsExists === false) {
      return ({'backgroundColor': 'red', 'color': '#fff'});
    } else if (propIsExists === true) {
      return ({'backgroundColor': '#00B200', 'color': '#fff'});
    } else {
      return ({'backgroundColor': '#E3E8EE', 'color': '#5a5a5a'});
    }
  }

  onChange(e) {
    const {checkEmailRegistration} = this.props;
    if (e.target.name == 'email') {
        checkEmailRegistration(e.target.value);
    }  else if (e.target.name == 'name') {
      if (e.target.value.length >= 3) {
        this.setState({nameVerified: true});
      } else {
        this.setState({nameVerified: false});
      }
    } else if (e.target.name == 'password') {
      this.setState({password: e.target.value});
      if (e.target.value.length >= 6) {
        this.setState({passwordVerified: true});
        this.setState({isEnterPassword: true});
      } else if (e.target.value.length < 6) {
        this.setState({passwordVerified: false});
        this.setState({isEnterPassword: false});
      }
    }  else if (e.target.name == 'repeat-password') {
      if (e.target.value === this.state.password) {
        this.setState({repeatPasswordVerified: true});
      } else {
        this.setState({repeatPasswordVerified: false});
      }
    }
  }

  componentWillMount() {
    const {clearCredentials} = this.props;
    clearCredentials();
  }

  render() {
    const {clientError, userExists} = this.props;

    return (
      <div className={SCSS_ROOT}>
        <form className={SCSS_LOGIN_FORM} onSubmit={ (e) => this.signUp(e) }>
          <div className={SCSS_HEADER}>
            Registration
          </div>
          <div className={SCSS_BODY}>
            <div className={SCSS_INPUT}>
              <div className={SCSS_ICON_WRAP} style={ this.isExists(userExists) } >
                <i className='fa fa-envelope'/>
              </div>
              <input type="email" name="email" placeholder="Enter your Email" onChange={(e) => this.onChange(e)}/>
            </div>
            <div className={SCSS_INPUT}>
              <div className={SCSS_ICON_WRAP} style={ this.isNotExists(this.state.nameVerified) } >
                <i className='fa fa-user-circle'/>
              </div>
              <input type="text" name="name" placeholder="Enter your Name" onChange={(e) => this.onChange(e)}/>
            </div>
            <div className={SCSS_INPUT}>
              <div className={`${SCSS_ICON_WRAP} ${SCSS_ICON_PASSWORD}`} style={ this.isNotExists(this.state.passwordVerified) }>
                <i className='fa fa-lock'/>
              </div>
              <input type="password" name="password" placeholder="Enter your password" onChange={(e) => this.onChange(e)}/>
            </div>
            <div className={SCSS_INPUT}>
              <div className={`${SCSS_ICON_WRAP} ${SCSS_ICON_PASSWORD}`} style={ this.isNotExists(this.state.repeatPasswordVerified) }>
                <i className='fa fa-unlock-alt'/>
              </div>
              <input type="password" name="repeat-password" placeholder="Repeat your password" onChange={(e) => this.onChange(e)}/>
            </div>
            <div className={SCSS_CLIENT_ERROR} style={{'display': (clientError)?'flex':'none'}}>
              <div className={SCSS_ICON_WRAP}>
                <i className='fa fa-times'/>
              </div>
              <div className={SCSS_MESSAGE}>{clientError}</div>
            </div>
            <div className={SCSS_BTN}>
              <input type="submit" value="Sign Up" disabled={( userExists || !this.state.nameVerified || !this.state.passwordVerified || !this.state.isEnterPassword || !this.state.repeatPasswordVerified )?'disabled':''}/>
            </div>
            <div className={SCSS_SIGNUP}>
              If you have account, please <Link to="/login">Login</Link>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

export default RegistrationForm;