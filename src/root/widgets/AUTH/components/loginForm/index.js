import React, {Component} from "react";
import {Link} from 'react-router';
import {connect} from "react-redux";

// Actions
import {signIn, checkEmailLogin, clearCredentials} from '../../action';

import {
  SCSS_ROOT,
  SCSS_LOGIN_FORM,
  SCSS_HEADER,
  SCSS_BODY,
  SCSS_USER_LOGO,
  SCSS_INPUT,
  SCSS_ICON_WRAP,
  SCSS_ICON_PASSWORD,
  SCSS_CLIENT_ERROR,
  SCSS_MESSAGE,
  SCSS_BTN,
  SCSS_SIGNUP
} from './scss/index.scss';


@connect(({auth}) => ({...auth}), {signIn, checkEmailLogin, clearCredentials})

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEnterPassword: false
    }
  }

  userLogo() {
    return (
      <i className="fa fa-user-o fa-4x"/>
    )
  }

  userExists() {
    const { userExists } = this.props;
    if (userExists) {
      return ({'display': 'flex'});
    }
  }

  userVerified() {
    const { userExists } = this.props;
    if (userExists) {
      return ({'backgroundColor': '#00B200', 'color': '#fff'});
    }
  }

  signIn(e) {
    const {signIn} = this.props;
    e.preventDefault(e);
    let email = e.target.email.value;
    let password = e.target.password.value;
    signIn(email, password);
  }


  onChange(e) {
    const {checkEmailLogin} = this.props;
    if (e.target.name == 'email') {
      checkEmailLogin(e.target.value);
    } else if (e.target.name == 'password') {
      if (e.target.value.length >= 6) {
        this.setState({isEnterPassword: true});
      } else {
        this.setState({isEnterPassword: false});
      }
    }
  }

  componentWillMount() {
    const {clearCredentials} = this.props;
    clearCredentials();
  }

  render() {
    const {clientError} = this.props;

    return (
      <div className={SCSS_ROOT}>
        <form className={SCSS_LOGIN_FORM} onSubmit={ (e) => this.signIn(e) }>
          <div className={SCSS_HEADER}>
            Administrator Login
          </div>
          <div className={SCSS_BODY}>
            <div style={this.userExists()} className={SCSS_USER_LOGO}>
              { this.userLogo() }
            </div>
            <div className={SCSS_INPUT}>
              <div className={SCSS_ICON_WRAP} style={ this.userVerified() }>
                <i className='fa fa-envelope'/>
              </div>
              <input type="email" name="email" placeholder="Enter your Email" onChange={(e) => this.onChange(e)}/>
            </div>
            <div className={SCSS_INPUT}>
              <div style={this.userExists()} className={`${SCSS_ICON_WRAP} ${SCSS_ICON_PASSWORD}`}>
                <i className='fa fa-lock'/>
            </div>
              <input style={this.userExists()} type="password" name="password" placeholder="Enter your password" onChange={(e) => this.onChange(e)}/>
            </div>
            <div className={SCSS_CLIENT_ERROR} style={{'display': (clientError)?'flex':'none'}}>
              <div className={SCSS_ICON_WRAP}>
                <i className='fa fa-times'/>
              </div>
              <div className={SCSS_MESSAGE}>{clientError}</div>
            </div>
            <div className={SCSS_BTN}>
              <input type="submit" value="Sign In" disabled={(!this.state.isEnterPassword)?'disabled':''}/>
            </div>
            <div className={SCSS_SIGNUP}>
              <Link to="/registration">Create an account</Link>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

export default LoginForm;