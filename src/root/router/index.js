import React from 'react';
import {Router, IndexRoute, Route, browserHistory} from 'react-router';
import cookies from 'js-cookie';
import axios from 'axios';

import App from '../container/App';
import Home from '../container/Pages/Home';
import SignIn from '../container/Pages/SignIn';
import SignUp from '../container/Pages/SignUp';
import NotFound from '../container/Pages/NotFound';

export default (
    [
        <Router path='/' component={App} onEnter={onEnterBase}>
            <IndexRoute component={Home} onEnter={onEnter}/>
            <Route path="/login" component={SignIn}/>
            <Route path="/registration" component={SignUp}/>
        </Router>,
        <Route path='*' component={NotFound}/>
    ]
);

function onEnterBase(nextState, replace, callback) {
  axios.interceptors.request.use((config) => {

    if (cookies.get('access_token')) {
      config.headers = {Authorization: `Bearer ${cookies.get('access_token')}`};
      return config;
    }

    return config;
  }, function (error) {
    const ErrorTest = JSON.stringify(error);
    const ErrorJson = JSON.parse(ErrorTest);

    return Promise.reject(ErrorJson, error);
  });

  axios.interceptors.response.use((response) => {
    return response;
  }, function (error) {

    const ErrorTest = JSON.stringify(error);
    const ErrorJson = JSON.parse(ErrorTest);
    const {response} = ErrorJson;
    if ((response.status === 401)) {
      cookies.remove('access_token');
      replace('/login');
    }
    return Promise.reject(ErrorJson, error);

  });

  return callback();
}

function onEnter(nextState, replace, callback) {
  const accessToken = cookies.get('access_token');

  if (!accessToken) {
    replace('/login');

    return callback();
  }

  return callback();
}