import React, {Component} from "react";
import {connect} from "react-redux";
import {RegistrationForm} from '../../../widgets';

// @connect(({page}) => ({...page}), {})

class SignUp extends Component {

    render() {
        return (
            <div>
                <RegistrationForm/>
            </div>
        )
    }
}

export default SignUp;

